#!/bin/sh

VERSION="$(dpkg-parsechangelog | grep-dctrl -ensVersion -FSource .)"
DATE="$(date --utc --date="`dpkg-parsechangelog -SDate`" '+%Y-%m-%d %H:%M:%S %z')"

echo "commit=''"
echo "date='$DATE'"
echo "modified=False"
